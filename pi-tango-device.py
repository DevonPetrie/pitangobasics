"""
Run this python3 script on your local machine like this:
  python3 pi-tango-device.py test -v4 -nodb -port 45678 -dlist my/pi/1

Then connect to the Device from the same machine like this in iTango:
  import platform
  import tango

  dp = tango.DeviceProxy(f"tango://localhost:45678/my/pi/1#dbase=no")
"""
from tango import (
  DevDouble,
  DevVoid,
  DevState,
  AttrWriteType,
  DevFailed
)

from tango.server import (
  Device,
  attribute,
  run,
  command
)

from gpiozero import CPUTemperature, Servo
from math import pi

import time
import math
import psutil
import queue
import RPi.GPIO as GPIO
import threading

# LEDs indexed left to right, values are the GPIO pins
LEDS = [16, 20, 18, 24, 23]
SERVO_GPIO = 21

def move_servo(servo, value, move_cb=None):
   if value < -1 or value > 1:
     print(f"Invalid value ({-1 * value}) given. Must be between -1 and 1.")
     return
   else:
     update_rate = 0.05
     update_value = 0.05

     curr_val = servo.value
     direction = 1 if value > curr_val else -1

     print(direction)

     while (direction == 1 and curr_val < value) or (direction == -1 and curr_val > value):
       curr_val = curr_val + update_value * direction

       if (direction == 1 and curr_val > value) or (direction == -1 and curr_val < value):
         curr_val = value

       print(curr_val)

       servo.value = curr_val

       if move_cb is not None:
          move_cb()

       time.sleep(update_rate)

     servo.value = value

def control_led(led_index: int, onOrOff: bool) -> str:
    global LEDS

    if led_index >= 0 and led_index < len(LEDS):
      GPIO.output(LEDS[led_index], GPIO.HIGH if onOrOff else GPIO.LOW)

      return f"Successfully turned {'on' if onOrOff else 'off'} LED {led_index}."
    else:
       return f"Invalid LED Index ({led_index}). Only allowed between 0 and {len(LEDS)}."

def get_led_state(led_index: int) -> bool:
    global LEDS

    if led_index >= 0 and led_index < len(LEDS):
      led_state = GPIO.input(LEDS[led_index])

      return led_state == GPIO.HIGH

def control_all_leds(onOrOff: bool):
    global LEDS

    for i in range(len(LEDS)):
      control_led(i, GPIO.HIGH if onOrOff else GPIO.LOW)

    return f"Successfully turned all LEDS {'on' if onOrOff else 'off'}."

def load_animation(runs: int):
    global LEDS

    num_leds = len(LEDS)

    # Turn off all LEDS
    for i in range(num_leds):
      control_led(i, False)

    for run in range(runs):
      for i in range(num_leds):
        control_led(i, True)
        time.sleep(0.2)
        control_led(i, False)

def run_as_thread(callback, name, args):
    if args is not None:
      animation_thread = threading.Thread(target=callback, args=args, name=name)
      animation_thread.start()
    else:
      animation_thread = threading.Thread(target=callback, name=name)
      animation_thread.start()

def run_load_animation(num_animations):
    run_as_thread(load_animation, "Load Animation", (num_animations,))

def run_startup_animation():
    global LEDS

    def anim():
      global LEDS

      control_all_leds(False)      
      
      for i in range(math.floor(len(LEDS)/2)):
        control_led(i, True)
        control_led(len(LEDS)-1-i, True)

        time.sleep(1)

      control_led(math.floor(len(LEDS)/2), True)

      time.sleep(1)
      control_all_leds(False)
 
    run_as_thread(anim, "Startup Animation", None)

class PiTangoDevice(Device):
  def init_device(self):
    super().init_device()
    self.set_state(DevState.ON)
    
    self.cpu = CPUTemperature()

    GPIO.setmode(GPIO.BCM)

    for led in LEDS:
      GPIO.setup(led, GPIO.OUT)

    self.servo = Servo(SERVO_GPIO)
    self.servo.value = 0.0

    self.command_requests = queue.Queue() 
    self.command_requests_lock = threading.Lock()

    run_startup_animation()

    for attribute_name in ["servoPosition", "led0", "led1", "led2", "led3", "led4", "commandingUser"]:
      self.set_change_event(attribute_name, True, False)

  def check_commanding_user(self, method):
    def wrapper(*args, **kwargs):
      client_name = self.get_client().client_name

      print("Current commanding user:", self.get_commanding_user())
      print("Client name:", client_name)

      print(self.get_commanding_user() == client_name)
      if self.get_commanding_user() == client_name:
        result = method(*args, **kwargs)  # Call the original method
        return result

    return wrapper

  @attribute(dtype=DevDouble, max_alarm=80, min_alarm=-1)
  def cpuTemp(self) -> DevDouble:
    return self.cpu.temperature

  @attribute(dtype=DevDouble)
  def cpuUsage(self) -> DevDouble:
    return psutil.cpu_percent()

  @attribute(dtype=DevDouble, max_alarm=0.95, min_alarm=-0.95, access=AttrWriteType.READ_WRITE)
  def servoPosition(self) -> DevDouble:
    return -self.servo.value

  @servoPosition.write
  def servoPosition(self, val):
    return self.move_servo(val)

  @attribute(dtype=bool, access=AttrWriteType.READ_WRITE)
  def led0(self) -> bool:
    return get_led_state(0)

  @led0.write
  def led0(self, val):
    message = control_led(0, val)
    self.push_change_event("led0", get_led_state(0))
    return message

  @attribute(dtype=bool, access=AttrWriteType.READ_WRITE)
  def led1(self) -> bool:
    return get_led_state(1)

  @led1.write
  def led1(self, val):
    message = control_led(1, val)
    self.push_change_event("led1", get_led_state(1))
    return message


  @attribute(dtype=bool, access=AttrWriteType.READ_WRITE)
  def led2(self) -> bool:
    return get_led_state(2)

  @led2.write
  def led2(self, val):
    message = control_led(2, val)
    self.push_change_event("led2", get_led_state(2))
    return message

  @attribute(dtype=bool, access=AttrWriteType.READ_WRITE)
  def led3(self) -> bool:
    return get_led_state(3)

  @led3.write
  def led3(self, val):
    message = control_led(3, val)
    self.push_change_event("led3", get_led_state(3))
    return message

  @attribute(dtype=bool, access=AttrWriteType.READ_WRITE)
  def led4(self) -> bool:
    return get_led_state(4)

  @led4.write
  def led4(self, val):
    message = control_led(4, val)
    self.push_change_event("led4", get_led_state(4))
    return message

  @attribute(dtype=str)
  def commandingUser(self) -> str:
    return self.get_commanding_user()

  # @attribute(max_alarm=100, min_alarm=-1)
  # def my_rw_attribute(self) -> DevDouble:
  #   return self.__my_rw_attribute_value

  # @my_rw_attribute.write
  # def my_rw_attribute(self, value: DevDouble=None) -> DevVoid:
  #   self.__my_rw_attribute_value = value
  #   self.push_change_event("my_rw_attribute", self.__my_rw_attribute_value)

  @command(dtype_in = DevDouble, dtype_out = DevDouble)
  def pi(self, value) -> DevDouble:
    return pi * value

  @command(dtype_in=int, dtype_out=str)
  def turnOnLed(self, led_index: int) -> str:
    message = control_led(led_index, True)
    self.push_change_event(f"led{led_index}", get_led_state(led_index))
    return message

  @command(dtype_in=int, dtype_out=str)
  def turnOffLed(self, led_index: int) -> str:
    message = control_led(led_index, False)
    self.push_change_event(f"led{led_index}", get_led_state(led_index))
    return message

  def move_servo(self, val):
    if val < -1 or val > 1:
      return "Invalid value given. Must be between -1 and 1."
    else:
      def move_cb():
        self.push_change_event("servoPosition", -self.servo.value)

      move_servo(self.servo, -val, move_cb)

      return f"Successfully moved servo to position {val}."

  @command(dtype_in=DevDouble, dtype_out=str)
  def moveServo(self, val) -> str:
    return self.move_servo(val)

  @command(dtype_in=int, dtype_out=str)
  def animateLoad(self, num_animations: int) -> str:
    run_load_animation(num_animations)
    return "Animation started"

  def get_commanding_user(self):
    if not self.command_requests.empty():
      return self.command_requests.queue[0]
    else:
      return "None"

  @command(dtype_in=str, dtype_out=int)
  def requestCommand(self, client_name) -> int:
    print("here")
    print(self.command_requests_lock)
    with self.command_requests_lock:
      if client_name in self.command_requests.queue:
        for index, name in enumerate(self.command_requests.queue):
          if client_name == name:
            return index

      self.command_requests.put(client_name)

      position_in_queue = self.command_requests.qsize()-1

      if position_in_queue == 0:
        self.push_change_event("commandingUser", self.get_commanding_user())

      return position_in_queue

  @command(dtype_in=str)
  def relinquishCommand(self, client_name) -> DevVoid:
    with self.command_requests_lock:
      if self.get_commanding_user() == client_name:
        self.command_requests.get()

        self.push_change_event("commandingUser", self.get_commanding_user())

  @command(dtype_in=int, dtype_out=bool)
  def clearCommandQueue(self, val) -> bool:
    with self.command_requests_lock:
      if val == 4398:
        while not self.command_requests.empty():
          self.command_requests.get()

        self.push_change_event("commandingUser", self.get_commanding_user())
        return True
      return False

def main(args=None, **kwargs):
  return run((PiTangoDevice,), **kwargs)

if __name__ == "__main__":
  main()
